# JGI template #

This repository is a template for other JGI repositories using gitlab to build docker images.

This is very much a work-in-progress, but we need something as a starting point.

When you import this repository, please update the **README** as one of your first actions!

Note that the **Dockerfile** and **.gitlab-ci.yaml** files aren't exactly world-class at the moment, they should improve with time. In particular, the **Dockerfile** is based on a heavy image, and is not at all optimal, but at least it gives something for gitlab to build.

Also, it's based on an image that is reasonably close to Genepool/Denovo, so if you can build software there you can probably build it in this container.

There's also a skeleton **.gitignore** and **.dockerignore** file for you to play with.

There's a script in the **util** directory. **util/cilint**. You can use that to check your **.gitlab-ci.yml** file before committing, if you like. Just run it, from the top-level directory, it doesn't take any options.
